# todo-list-app

Use this Todo LIST API Documentation: https://documenter.getpostman.com/view/8858534/SW7dX7JG

CRUD (Create, Read, Update, Delete) for User and Task

### Requirement
#### Page
1. [Register](#register)
2. [Login](#login)
3. [All Task](#all-task)
4. [Active Task](#active-task)
5. [Completed Task](#completed-task)
6. [Profile (Get, Update, Delete)](#profile)

###### register <a name="register"></a>
- after register, go to all task list page
- save token on asyncstorage
- if token already on asyncstorage, redierct to all task page

###### login <a name="login"></a>
- after login, go to all task list page
- save token on asyncstorage
- if token already on asyncstorage, redierct to all task page

###### all task <a name="all-task"></a>
- get all data for all task
- we can change from all task status and send an Update to API after 3 sec
- we can delete task

###### active task <a name="active-task"></a>
- get all data only for active task
- we can change from active task status and send an Update to API after 3 sec
- we can delete task

###### completed task <a name="completed-task"></a>
- get all data only for completed task
- we can change from completed task status and send an Update to API after 3 sec
- we can delete task

###### profile <a name="completed-task"></a>
- we can get, update and delete profile
- show logout button and  use an API and redirect to login page