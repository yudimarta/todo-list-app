import { combineReducers } from 'redux'

import authStoreReducer from './authStoreReducer'
import getAllTaskReducer from './getAllTaskReducer'

const reducers = {
    authStore : authStoreReducer,
    getUserStore : authStoreReducer,
    registerStore : authStoreReducer,
    getAllTaskStore : getAllTaskReducer,
}

const rootReducer = combineReducers(reducers)

export default rootReducer