const defaultState = {
    payload : {},
    isLoading : false,
    error : {}
}

export default (state = defaultState, action = {}) => {
    switch (action.type){
        case 'FETCH_GETALLTASK_REQUEST': {
            return {
              ...state,
              isLoading: true
            };
          }
      
          case 'FETCH_GETALLTASK_SUCCESS': {
            return {
              ...state,
              payload: action.payload,
              isLoading: false
            };
          }
      
          case 'FETCH_GETALLTASK_FAILED': {
            return {
              ...state,
              payload: {},
              error: action.error,
              isLoading: false
            };
          }
          default:
            return state;
    }
}