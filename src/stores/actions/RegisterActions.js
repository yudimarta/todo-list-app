import register from '../../api/authRegister'

function fetchRegister(name, email, password, age){
    return async (dispatch) => {
        dispatch({
            type: 'FETCH_LOGIN_REQUEST'
        })

        try {
            const result = await register(name, email, password, age)

            if (result.status === 201){
                dispatch({
                    type : 'FETCH_LOGIN_SUCCESS',
                    payload : result.data
                })
            } else {
                dispatch({
                    type : 'FETCH_LOGIN_FAILED',
                    payload : result.data
                })
            }
        } catch (error){
            dispatch({
                type : 'FETCH_LOGIN_FAILED',
                error : error
            })
        }
    }
}

export default fetchRegister