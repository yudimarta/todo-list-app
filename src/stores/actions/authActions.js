import login from '../../api/authLogin'

function fetchLogin(email, password){
    return async (dispatch) => {
        dispatch({
            type: 'FETCH_LOGIN_REQUEST'
        })

        try {
            const result = await login(email, password)

            if (result.status === 200){
                dispatch({
                    type : 'FETCH_LOGIN_SUCCESS',
                    payload : result.data
                })
            } else {
                dispatch({
                    type : 'FETCH_LOGIN_FAILED',
                    payload : result.data
                })
            }
        } catch (error){
            dispatch({
                type : 'FETCH_LOGIN_FAILED',
                error : error
            })
        }
    }
}

export default fetchLogin