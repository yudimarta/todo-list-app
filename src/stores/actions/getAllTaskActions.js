import getAllTask from '../../api/getAllTask'

function fetchGetAllTask(token){
    return async (dispatch) => {
        dispatch({
            type: 'FETCH_GETALLTASK_REQUEST'
        })

        try {
            const result = await getAllTask(token)

            if (result.status === 200){
                dispatch({
                    type : 'FETCH_GETALLTASK_SUCCESS',
                    payload : result.data
                })
            } else {
                dispatch({
                    type : 'FETCH_GETALLTASK_FAILED',
                    payload : result.data
                })
            }
        } catch (error){
            dispatch({
                type : 'FETCH_GETALLTASK_FAILED',
                error : error
            })
        }
    }
}

export default fetchGetAllTask