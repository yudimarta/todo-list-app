import React, { useState, useEffect} from 'react'
import { Modal, TouchableOpacity, Text, View, TextInput } from 'react-native'
import styles from '../Styling/Styling'
import Icons from 'react-native-vector-icons/Ionicons';

function ModalUpdate(props){
    const { buttonText, placeholderText, icon, invert, disable, onDone, onDelete, setModal, ...others} = props
    const [modalVisible, setModalVisible] = useState(false)

    return(
          <View> 
            <Modal
              animationType="slide"
              transparent={true}
              {...others}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalText}>Task Confirmation</Text>
                  <View style={{flexDirection : 'row', justifyContent:'space-between', }}>
                    <TouchableOpacity
                        style={{ ...styles.dualButton, backgroundColor: "#1FCC79", marginTop : 12, marginRight: 10}}
                        onPress={onDone}
                        {...others}
                    >
                      <Text style={styles.textStyle}>  Done  </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                        style={{ ...styles.dualButton, backgroundColor: "#FFFFFF", borderColor : 'red', borderWidth: 1, marginTop : 12 }}
                        onPress={onDelete}
                        {...others}
                    >
                      <Text style={{...styles.textStyle, color:'red'}}>Delete</Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </Modal>
          </View>
    )
}

export default ModalUpdate