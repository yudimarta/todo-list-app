import React, { useState, useEffect} from 'react'
import { Modal, TouchableOpacity, Text, View, TextInput } from 'react-native'
import styles from '../Styling/Styling'
import Icons from 'react-native-vector-icons/Ionicons';

function ModalWithTextBox(props){
    const { buttonText, placeholderText, icon, invert, disable, ...others} = props
    const [modalVisible, setModalVisible] = useState(false)

    return(
          <View> 
            <Modal
              animationType="slide"
              transparent={true}
            //   visible={modalVisible}
            //   onRequestClose={() => {
            //   setModalVisible(!modalVisible);
            //   }} 
              {...others}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  {/* <Text style={styles.modalText}>Add New Task</Text> */}
                  <TextInput 
                    placeholder={placeholderText}
                    style={{textAlign : 'center'}}
                    {...others}/>
                  <TouchableOpacity
                    style={{ ...styles.openButton, backgroundColor: "#2196F3" }}
                    // onPress={() => {
                    //   handleAdd(description)
                    //   setModalVisible(!modalVisible);
                    // }}
                    {...others}
                  >
                    <Text style={styles.textStyle}>{buttonText}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>
    )
}

export default ModalWithTextBox