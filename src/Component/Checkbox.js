import React, { useState, useEffect } from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import CheckBox from '@react-native-community/checkbox';
import styles from '../Styling/Styling'
import Icons from 'react-native-vector-icons/Ionicons';

function Checkbox(props){
    const { text, invert, check, ...others} = props
    const [toggleCheckBox, setToggleCheckBox] = useState(false)
    const [textcolor, setTextColor] = useState('#9FA5C0')

    useEffect(() => {
        setToggleCheckBox(check)
        setTextColor(toggleCheckBox ? '#2E3E5C' : '#9FA5C0')
    })

    return(
            <View style={styles.checkBox}>    
                <CheckBox
                    disabled={true}
                    value={toggleCheckBox}
                    tintColors={{ true : '#1FCC79', false : '#E3FFF1' }}
            />      
                <Text style={{color:textcolor, fontSize:15}}>{text}</Text>
            </View>    
    )
}

export default Checkbox