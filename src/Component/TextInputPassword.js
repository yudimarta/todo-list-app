import React,{ useState, useEffect } from 'react'
import { TouchableOpacity, Text, View, TextInput } from 'react-native'
import Icons from 'react-native-vector-icons/Ionicons';
import styles from '../Styling/Styling'

function TextInputPassword(props){
    const { text, icon, ...others} = props
    const [icon2, setIcon2] = useState('eye-outline')
    const [visible, setVisible] = useState(true)
    const [count, setCount] = useState(0)

    const checkVisible = () => {
        setCount(count + 1)
        if (count % 2 == 0){
            setVisible(false)
            setIcon2('eye-off-outline')
        } else {
            setVisible(true)
            setIcon2('eye-outline')
        }
    }

    return(
        <View style={[styles.textInputPassword, props.customStyle]} {...others}>
            <Icons style={{marginLeft : 20}} name={icon} size={20} color='#1FCC79'/>
            <TextInput 
                placeholder={text}
                placeholderTextColor='#9FA5C0'
                secureTextEntry={visible}
                style={{
                    alignSelf : 'center',
                    height : 56,
                    width : 250,
                    paddingLeft : 10}} {...others}/>
            <Icons style={{paddingRight : 10}} name={icon2} size={20} color='#9FA5C0' onPress={() => checkVisible()}/> 
        </View>
    )
}

export default TextInputPassword