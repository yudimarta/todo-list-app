import { Text, Card, Divider } from 'react-native-elements';
import React, { useState, useEffect } from 'react'
import { StyleSheet, View, Button, TouchableHighlight, FlatList, TouchableOpacity, Image, ImageBackground, TextInput} from 'react-native'
import { CardStyle } from '../Styling/Styling'
import Icons from 'react-native-vector-icons/Ionicons';

function Card(props){
    const { text, invert, check, ...others} = props

    renderItem = ({ item }) => {
        return(
          <TouchableOpacity style={styles.bodyBackgroundColor} onPress={() => {
            console.log('yang dibuka dari onPress: ', item.url)
            this.moveScreen({url : item.url})
            }}>
    
            <Card containerStyle={styles.cardBackgroundColor}>
              <Card.Image source = {{uri : item.urlToImage}} >
                <Text style = {styles.title}> {item.title} </Text>
              </Card.Image> 
          
              <Text style={styles.description}>
                {item.description}
              </Text>
              <Divider style={{ backgroundColor: '#DFE6E9' }} />
              <View style={styles.authorBar}>
                <Text style={styles.authorText}>
                  {item.author}  |  {item.source.name.toUpperCase()}  |  {moment(item.publishedAt).format('MMM DD, YYYY')}
                </Text>
                <Icons name="heart-outline" color={'#DFE6E9'} size={ 14 } style={{margin : 5}}/>
              </View>
            </Card>
          </TouchableOpacity>
        )
      }
}