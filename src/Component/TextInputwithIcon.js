import React, { useState, useEffect } from 'react'
import { TouchableOpacity, Text, View, TextInput } from 'react-native'
import Icons from 'react-native-vector-icons/Ionicons';
import styles from '../Styling/Styling'

function TextInputwithIcon(props){
    const { text, icon, ...others} = props


    return(
        <View style={[styles.textInput, props.customStyle]} {...others}>
            <Icons style={{marginLeft : 50}} name={icon} size={20} color='#1FCC79'/>
            <TextInput 
                placeholder={text}
                placeholderTextColor='#9FA5C0'
                style={{
                    alignSelf : 'center',
                    height : 56,
                    width : 327,
                    paddingLeft : 20}} {...others} 
                    />     
        </View>
    )

}

export default TextInputwithIcon