import React from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import styles from '../Styling/Styling'
import Icons from 'react-native-vector-icons/Ionicons';

function FloatingButton(props){
    const { buttonText, icon, invert, disable, ...others} = props

    return(
        <TouchableOpacity style={[
            styles.button,
            invert && styles.invert,
            props.customStyle
            ]}
            {...others}>
            <View style={{
                flexDirection:'row',
                justifyContent : 'center',
                alignItems: 'center'}}>    
                <Icons name='add'size={20} color='#FFFFFF'/>       
                {/* <Text style={styles.textButton}>{buttonText}</Text> */}
            </View>    
        </TouchableOpacity>
    )
}

export default FloatingButton