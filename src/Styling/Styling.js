import { StyleSheet } from 'react-native'
import { color } from 'react-native-reanimated'

const styles = StyleSheet.create({
    container : {
        flex : 1
    },
    button : {
        backgroundColor : '#1FCC79',
        borderRadius : 32,
        marginTop : 72,
        height : 54,
        width : 327,
        justifyContent : 'center',
        alignSelf : 'center'
    },
    invert : {
        backgroundColor : 'red'
    },
    mainText : {
        // alignSelf : 'center', 
        fontWeight : 'bold', 
        textAlign : 'center',
        fontSize : 22,
        letterSpacing : 0.5,
        lineHeight : 32,
        marginTop : 48,
        color : '#2E3E5C'
    },
    secondaryText : {
        // alignSelf : 'center', 
        fontWeight : '500', 
        textAlign : 'center',
        fontSize : 17,
        letterSpacing : 0.5,
        lineHeight : 25,
        marginTop : 16,
        color : '#9FA5C0'
    },
    textButton : {
        fontSize : 15,
        textAlign: 'center',
        color : '#FFFFFF',
        lineHeight : 18,
        fontWeight : 'bold'
    },
    textInput : {
        flexDirection:'row',
        justifyContent : 'center',
        alignItems: 'center',
        marginTop : 16,
        borderColor:'#D0DBEA',
        borderRadius : 32,
        borderWidth: 1,
        marginLeft : 24,
        marginRight : 24
    },
    textInputPassword : {
        flexDirection:'row',
        justifyContent : 'space-evenly',
        alignItems: 'center',
        marginTop : 16,
        borderColor:'#D0DBEA',
        borderRadius : 32,
        borderWidth: 1,
        marginLeft : 24,
        marginRight : 24
    },
    checkBox : {
        flexDirection:'row',
        marginLeft : 24,
        marginTop : 16,
        alignItems: 'center'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width : 125
      },
      dualButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width : 70
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center",
        color : '#2E3E5C'
      }
})

export const CardStyle = StyleSheet.create({
    tulisan : {
        fontWeight: 'bold', 
        textAlign: 'center',
        color: '#B0A8B9',
        fontSize: 34,
        marginTop: 24
    },
    tulisan2 : {
        textAlign: 'center',
        color: '#B0A8B9',
        fontSize: 16,
        marginTop: 12
    },
    centerItem : {
        alignItems: 'center', 
        flex:1, 
        justifyContent:'center'
    },
    title : {
        marginVertical : 60,
        textAlign : 'center',
        color : 'white',
        fontWeight : 'bold',
        fontSize : 16,
        textShadowColor: '#000000',
		textShadowOffset: { width: 3, height: 3 },
        textShadowRadius: 5, 
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    description : {
        marginBottom: 10, 
        marginTop : 10, 
        color: '#b2bec3'
    },
    authorBar : {
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        backgroundColor: '#4B4453'
    },
    authorText : {
        margin: 5,
        color: '#b2bec3',
        fontSize: 8
    },
    bodyBackgroundColor : {
        backgroundColor:'#332940',
        flex : 1
    },
    cardBackgroundColor : {
        backgroundColor:'#4B4453'
    },
    searchBar : {
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    titleText : {
        color : '#FFFFFF',
        fontWeight : 'bold',
        fontSize : 20,
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    textInput : {
        flexDirection: 'row', 
        justifyContent: 'center'
    },
    textInputCard : {
        backgroundColor:'#4B4453', 
        height:70
    },
    searchCardBackgroundColor : {
        backgroundColor:'#332940'
    }
})

export default styles