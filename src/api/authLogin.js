import fetchUpdate from '../Utils/fetchUpdate'


async function login(email,password){
    try {
        const result = await fetchUpdate('POST','', 'user/login','', {email, password})
        console.log(result)
        return result
    } catch (error){
        alert('Login Failed, ' +  error)
    }
}

export default login