import fetchUpdate from '../Utils/fetchUpdate'

async function getAllTask(token){
    try {
        const result = await fetchUpdate('GET', token, 'task/', '', {})
        console.log(result)
        return result
    } catch (error){
        throw error
    }
}

export default getAllTask