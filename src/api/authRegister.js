import fetchUpdate from '../Utils/fetchUpdate'

async function register(name, email,password, age){
    try {
        const result = await fetchUpdate('POST','', 'user/register','', {name, email, password, age})
        console.log(result)
        return result
    } catch (error){
        alert('Register Failed, ' +  error)
    }
}

export default register