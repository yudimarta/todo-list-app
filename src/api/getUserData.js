import fetchUpdate from '../Utils/fetchUpdate'

async function getUserData(token){
    try {
        const result = await fetchUpdate('GET', token, 'user/me', '', {})
        console.log(result)
        return result
    } catch (error){
        throw error
    }
}

export default getUserData