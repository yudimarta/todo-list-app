import React, { Component, useState, useEffect } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, TextInput, ScrollView} from 'react-native'
import SecondaryText from '../Component/SecondaryText'
import TextInputwithIcon from '../Component/TextInputwithIcon'
import Button from '../Component/buttons'
import TextInputPassword from '../Component/TextInputPassword';
import Logo from '../assets/whattodologo.png'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';

import fetchLogin from '../stores/actions/authActions'


function SignIn(props){
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const { navigation } = props

    const handleLogin = async(val) => {
        try{
            await props.dispatchLogin(email,password)
            console.log('==handleLogin ', val);
        } catch (e) {
            console.log(e)
        }
    }

    useEffect(() => {
        props.authStore.payload.token = ''
        getData()
    }, [])

    const getData = async() => {
        try {
          const value = await AsyncStorage.getItem('@token')
          if(value !== null) {
            console.log('token : ' + value)
          } 
        } catch(e) {
            console.error(e)
        }
    }

    useEffect(() => {
        const {navigation} = props
        if(props.authStore.payload.token && props.authStore.payload.store !== ''){
            AsyncStorage.setItem('@token', props.authStore.payload.token)
            console.log(email,password)
            navigation.navigate('Home')
        }
    }, [props.authStore.payload])

    console.log('authStore: ', typeof(props.authStore.payload.token))

    return(
        <ScrollView>
            <Image source={Logo} resizeMode='center' style={{alignSelf:'center', height: 150, width : 150, marginTop : 100}}/>
            
            <SecondaryText text='Please enter your account here' customStyle={{ fontSize : 15, marginTop : 8}}/>

            <TextInputwithIcon text='Email or phone number' 
                icon='mail-outline'
                onChangeText={(value) => setEmail(value)}/>

            <TextInputPassword text='Password' 
                icon='lock-closed-outline' 
                icon2='eye-outline'
                onChangeText={(value) => setPassword(value)}/> 

            <Button 
                buttonText='Login'
                onPress={() => {handleLogin({email,password})}}/>     

            <View style={{
                flexDirection:'row',
                justifyContent : 'center',
                alignItems: 'center',
                marginTop : 16}}>
                    <SecondaryText 
                        text={`Don't have any account?`} 
                        customStyle={{color : '#2E3E5C', fontSize : 15, fontWeight : 'bold'}} />
                    <SecondaryText 
                        text='Sign Up'
                        onPress={() => navigation.navigate('SignUp')}
                        customStyle={{color : '#1FCC79',fontWeight : 'bold', fontSize : 15, marginLeft : 8}}
                    />      
                </View>    
        </ScrollView>
    )
}

function mapStateToProps(state) {
    return {
      authStore: state.authStore,
      isLoading: state.authStore.isLoading,
      //getUserStore : state.authStore.user
    }
  }
  
  function mapDispatchToProps(dispatch) {
    return {
      dispatchLogin: (email, password) => dispatch(fetchLogin(email, password)),
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(SignIn)