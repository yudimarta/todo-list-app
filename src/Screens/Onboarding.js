import React, { Component, useState, useEffect } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, TextInput} from 'react-native'
import Onboarding from '../assets/landingwhattodo.png'
import Button from '../Component/buttons'
import MainText from '../Component/MainText'
import SecondaryText from '../Component/SecondaryText'
import AsyncStorage from '@react-native-async-storage/async-storage';


function onBoarding(props){
    useEffect(() => {
        getData()
    }, [])

    const getData = async() => {
        try {
          const value = await AsyncStorage.getItem('@token')
        //   await AsyncStorage.setItem('@token','');
        //   await AsyncStorage.clear()
          if(value !== null) {
            console.log('token : ' + value)
            navigation.navigate(value ? 'Home' : 'SignIn')
          } 
        } catch(e) {
            console.error(e)
        }
    }

    

    const { navigation } = props
    return(
        <View style={{backgroundColor:'#E5E5E5', flex:1}}>
            <Image source={Onboarding} resizeMode='center' style={{alignSelf:'center', height: 250, width : 250, marginTop : 150}}/>
            
            <MainText text='Lots of Missed Activities?'/>
            
            <SecondaryText text={`Keep organize the unorganized \n in one Single Tap`}/>
        
            <Button 
                buttonText='Get Started'
                onPress={() => navigation.navigate('SignIn')}/>    

        </View>
    )
}

export default onBoarding