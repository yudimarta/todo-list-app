import React, { Component, useState, useEffect } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, TextInput, ScrollView, ScrollViewBase} from 'react-native'
import SecondaryText from '../Component/SecondaryText'
import TextInputwithIcon from '../Component/TextInputwithIcon'
import Button from '../Component/buttons'
import TextInputPassword from '../Component/TextInputPassword';
import Checkbox from '../Component/Checkbox';
import Logo from '../assets/whattodologo.png'
import fetchRegister from '../stores/actions/RegisterActions'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';


function SignUp(props){
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [age, setAge] = useState('')
    const [checkChar, setCheckChar] = useState(false)
    const [checkCharNum, setCheckCharNum] = useState(false)
    const [buttonDisable, setButtonDisable] = useState(true)
    const [buttonColor, setButtonColor] = useState('#D3D3D3')
    const { navigation } = props
    
    useEffect(() => {
        checkButton(checkChar, checkCharNum)
        console.log('pass1 : ',password)
        console.log('check1 : ',checkCharNum)
    })

    const handleRegister = async(val) => {
        try{
            props.dispatchRegister(name, email, password, age)
            console.log('==handleRegister ', val);
        } catch (e) {
            alert(e)
            console.log(e)
        }
    }

    useEffect(() => {
        const {navigation} = props
        if(props.authStore.payload.token && props.authStore.payload.store !== ''){
            AsyncStorage.setItem('@token', props.authStore.payload.token)
            console.log(name, age, email,password)
            navigation.navigate('Home')
        }
    }, [props.authStore.payload])

    const checkPasswordLenght = (pass) => {
        if (pass.length >= 6){
            setCheckCharNum(true)
            console.log(checkCharNum)
        } else {
            setCheckCharNum(false)
        }
    }

    const checkPasswordNum = (pass) => {
        if (pass.match(/\d+/g)){
            setCheckChar(true)
            console.log(checkChar)
        } else {
            setCheckChar(false)
        }
    }

    const checkButton = (char, num) => {
        if (char && num){
            setButtonDisable(false)
            setButtonColor('#1FCC79')
        } else {
            setButtonDisable(true)
            setButtonColor('#D3D3D3')
        }
    }


    console.log('pass2 : ',password)
    console.log('check2 : ',checkCharNum)
    console.log('ini token register : ', props.authStore.payload.token)
    return(
        //console.log(password)
        <ScrollView>
            <Image source={Logo} resizeMode='center' style={{alignSelf:'center', height: 150, width : 150, marginTop : 48}}/>
            
            <SecondaryText text='Please register your account here' customStyle={{marginTop : 8, fontSize : 15}}/>

            <TextInputwithIcon text='Full Name' icon='person-outline' autoFocus={true}  onChangeText={(value) => setName(value)}/>

            <TextInputwithIcon text='Age' icon='send-outline' onChangeText={(value) => setAge(value)}/>  

            <TextInputwithIcon text='Email Address' icon='mail-outline' onChangeText={(value) => setEmail(value)}/>

            <TextInputPassword text='Password' icon='lock-closed-outline' 
                onChangeText={(value) => {setPassword(value)
                    console.log('iki : ',value)
                    checkPasswordLenght(value)
                    checkPasswordNum(value)}} 
                />  

    
            <SecondaryText text='Your Password must contain : ' customStyle={{color : '#3E5481', marginTop : 24, textAlign : 'left', marginLeft : 24}}/>

            <Checkbox text='Atleast 6 characters' check={checkCharNum}/>

            <Checkbox text='Contains a number' check={checkChar} customStyle={{marginTop : 16}}/>
            
            <Button customStyle={{marginTop:23, backgroundColor : buttonColor}}
                buttonText='Sign Up'
                disabled={buttonDisable}
                onPress={() => handleRegister({name, email, password, age})}/>     

        </ScrollView>
    )
}

function mapStateToProps(state) {
    return {
      authStore: state.authStore,
      isLoading: state.authStore.isLoading,
    }
  }
  
  function mapDispatchToProps(dispatch) {
    return {
      dispatchRegister: (name, email, password, age) => dispatch(fetchRegister(name, email, password, age)),
    }
  }

export default connect(mapStateToProps, mapDispatchToProps)(SignUp)