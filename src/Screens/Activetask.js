import React, { Component, useState, useEffect } from 'react'
import { Modal, Alert, SafeAreaView, View, Button, TouchableHighlight, FlatList, TouchableOpacity, Image, ImageBackground, TextInput} from 'react-native'
import { Text, Card, Divider } from 'react-native-elements';
import Icons from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import fetchGetAllTask from '../stores/actions/getAllTaskActions'
import moment from 'moment'
import styles from '../Styling/Styling'
import fetchUpdate from '../Utils/fetchUpdate'
import ModalUpdate from '../Component/ModalUpdate'


function Activetask(props){
    const [list, setList] = useState([])
    const [refreshing, setRefreshing] = useState(true)
    const [loading, setLoading] = useState(true)
    const [taskId, setTaskId] = useState('')
    const [status, setStatus] = useState(false)
    const [text, setText] = useState('')
    const [auth, setAuth] = useState('')
    const [completed, setCompleted] = useState(true)
    const [modalVisible, setModalVisible] = useState(false);
    const [modalUpdateVisible, setModalUpdateVisible] = useState(false);
    const [modalDeleteVisible, setModalDeleteVisible] = useState(false);

    useEffect(() => {
      getData()
      let res = props.getAllTaskStore.payload.data.filter(obj => {return obj.completed == false})
      setList(res)
      setRefreshing(false)
      console.log('ini did mount')
      }, [])
    
    useEffect(() => {
      let res = props.getAllTaskStore.payload.data.filter(obj => {return obj.completed == false})
      setList(res)
  }, [props.getAllTaskStore.payload.data])  

  useEffect(() => {
    const interval = setInterval(() => {
      getData()
      //console.log('Interval triggered : ', auth);
    }, 5000);
    return () => {
      clearInterval(interval);
    }
  }, [])


    const getData = async() => {
        try {
          const value = await AsyncStorage.getItem('@token')
          if(value !== null) {
            console.log('token : ' + value)
            handleAllTask(value)
            setAuth(value)
          }
        } catch(e) {
            console.error(e)
        }
    }
    
    const handleAllTask = async(token) => {
      try{
        props.dispatchAllTask(token)
        console.log('handleAllTask : ', token)
      } catch(e) {
        alert(e)
        console.log(e)
      }
    } 
    
    //console.log('ini list dari getData : ', list.completed)
    console.log('ini status : ', status)

    const handleClick = (id, status) => {
      if (status) {
        setModalDeleteVisible(true)
        setTaskId(id)
      } else {
        setModalUpdateVisible(true)
        setTaskId(id)
      }
    }


    const handleDone = async(idx) => {
      try{
        const result = await fetchUpdate('PUT', auth, 'task/', idx, {completed})
        console.log(result)
        return result
      } catch(e){
        console.log(e)
      }
    }

    const handleDelete = async(idx) => {
      try{
        const result = await fetchUpdate('DELETE', auth,'task/',idx, {})
        return(result)
      } catch(e){
        console.log(e)
      }
    }
    
    
    const renderItem = ({ item }) => {
        return(
          <TouchableOpacity style={{backgroundColor : '#FFFFFF', flex : 1}} onPress={() => {
            console.log('yang dibuka dari onPress: ', item._id)
            handleClick(item._id, item.completed)
            }}>
            <Card containerStyle={{backgroundColor : item.completed ? '#1FCC79' :'#2E3E5C', borderRadius: 16, borderColor : '#FFFFFF'}}>
              <Text style={{marginBottom: 10, marginTop : 10, color: '#FFFFFF', fontSize : 20, fontWeight : 'bold'}}>
                {item.description}
              </Text>
              <Divider style={{ backgroundColor: '#DFE6E9' }} />
              <View style={{flexDirection: 'row', justifyContent: 'space-between', backgroundColor: item.completed ? '#1FCC79' :'#2E3E5C', marginTop : 8,  alignItems : 'center'}}>
                <Text style={{margin: 5, color: '#FFFFFF', fontSize: 10}}>
                {moment(item.createdAt).format('MMM DD, YYYY')}   |   {moment(item.createdAt || moment.now()).fromNow()}
                </Text>
                <View style={{flexDirection:'row', alignItems: 'center'}}>
                    <Text style={{color:'#FFFFFF', fontSize:15}}>{item.completed ? 'Completed' :'Incomplete'}</Text>
                    <Icons name={item.completed ? 'checkmark-circle' :'checkmark-circle-outline'} color={'#FFFFFF'} size={ 20 } style={{margin : 5}}/>
                </View>
              </View>
            </Card>
          </TouchableOpacity>
        )
      }
    
    
    
    return(
      <SafeAreaView style={{flex:1}}>
        <View>
        <ModalUpdate 
            visible={modalUpdateVisible} 
            onRequestClose={() => setModalUpdateVisible(!modalUpdateVisible)} 
            onDone = {() => {
              handleDone(taskId)
              setModalUpdateVisible(!modalUpdateVisible)}}
            onDelete={() => {
              handleDelete(taskId)
              setModalUpdateVisible(!modalUpdateVisible)}}
          />
          <FlatList
            data = {list}
            renderItem={renderItem}
            keyExtractor={list => list._id}
            refreshing={refreshing}
            onRefresh={getData}
          />
        </View>
      </SafeAreaView>
    )
}

function mapStateToProps(state) {
    return {
      getAllTaskStore: state.getAllTaskStore,
      isLoading : state.getAllTaskStore.isLoading,
    }
  }

  
  function mapDispatchToProps(dispatch) {
    return {
      dispatchAllTask: (token) => dispatch(fetchGetAllTask(token)),
    }
  }

export default connect(mapStateToProps, mapDispatchToProps) (Activetask)