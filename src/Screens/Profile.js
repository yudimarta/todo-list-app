import React, { Component, useState, useEffect } from 'react'
import { Modal, TouchableHighlight ,StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, TextInput, BackHandler, Alert} from 'react-native'
import { connect } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { Divider } from 'react-native-elements';
import fetchGetData from '../stores/actions/getDataActions'
import MainText from '../Component/MainText';
import styles from '../Styling/Styling'
import SecondaryText from '../Component/SecondaryText';
import Button from '../Component/buttons' 
import ModalWithTextBox from '../Component/ModalWithTextBox'
import fetchUpdate from '../Utils/fetchUpdate'
import Icons from 'react-native-vector-icons/Ionicons';
import ModalDelete from '../Component/modalDelete'

const profpic = { uri : 'https://conferenceoeh.com/wp-content/uploads/profile-pic-dummy.png' }

function Profile(props){
  const [token, setToken] = useState('')
  const [sum, setSum] = useState(0)
  const [not, setNot] = useState([])
  const [notCount, setNotCount] = useState(0)
  const [modalLogoutVisible, setModalLogoutVisible] = useState(false)
  const [modalNameVisible, setModalNameVisible] = useState(false)
  const [modalEmailVisible, setModalEmailVisible] = useState(false)
  const [modalAgeVisible, setModalAgeVisible] = useState(false)
  const [modalDelVisible, setModalDelVisible] = useState(false)
  const [name, setName] = useState('')
  const [email, setEmail] = useState('')
  const [age, setAge] = useState(0)
  const {navigation} = props
  

  useEffect(() => {
    getData()
    BackHandler.addEventListener('hardwareBackPress', handleBackButton);
  }, [])

  useEffect(() => {
    return () => {
      BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
    }
  }, [])

  useEffect(() => {
    setSum(props.getAllTaskStore.payload.count)
    setNot(props.getAllTaskStore.payload.data.filter(obj => {return obj.completed == false}))
    setNotCount(not.length)
}, [props.getAllTaskStore.payload])

  const handleDirect = async(token) => {
    try{
      props.dispatchDirect(token)
      console.log('handleDirect : ', token)
    } catch(e) {
      alert(e)
      console.log(e)
    }
  }

  const getData = async() => {
    try {
      const value = await AsyncStorage.getItem('@token')
      if(value !== null) {
        console.log('token : ' + value)
        setToken(value)
        handleDirect(value)
        // navigation.navigate('Home')
      }
    } catch(e) {
        console.error(e)
    }
  }

  onButtonPress = () => {
    BackHandler.removeEventListener('hardwareBackPress', handleBackButton);
  }
  
  handleBackButton = () => {
   Alert.alert(
       'Exit App',
       'Exiting the application?', [{
           text: 'Cancel',
           //onPress: () = > console.log('Cancel Pressed')},
           style: 'cancel'
       }, {
           text: 'OK',
           onPress : () => BackHandler.exitApp()
      
       }, ], {
           cancelable: false
       }
    )
    return true;
  }

  const removeItemValue = async() => {
    const {navigation} = props
    try {
        const value = await AsyncStorage.setItem('@token','');
        await AsyncStorage.clear()
        props.registerStore.payload = {}
        props.getAllTaskStore.payload.data = {}
        //props.getUserStore.payload = {}
        //props.authStore.payload = {}
        console.log('token : ' + value)
        navigation.navigate('SignIn')
        return true;
    }
    catch(exception) {
        return false;
    }
  }

  const updateProfile = async(data) => {
    try{
      const result = await fetchUpdate('PUT', token, 'user/me','', data)
      console.warn(result)
      return result
    } catch(e){
      console.warn(e)
    }
  }

  const removeAccount = async() => {
    try{
      await AsyncStorage.setItem('@token','');
      await AsyncStorage.clear()
      props.getAllTaskStore.payload.data = {}
      const result = await fetchUpdate('DELETE', token, 'user/me','', {})
      console.warn(result)
      navigation.navigate('SignIn')
      return result
    } catch(e){
      console.warn(e)
    }
  }

  // const updateProfile = (data) => {
  //   console.warn(data)
  // }
  


  console.log(props.user)
  console.log(sum)
  return(
    <View>
      <View style={{backgroundColor : '#006D81', height : 300, alignItems: 'center',  
        justifyContent:'center'}}>
        <Image
          source={profpic}
          style={{height: 150, width: 150, borderRadius:150/2}}
        />
        <View style={{flexDirection : 'row',  alignItems: 'center', justifyContent: 'space-between'}}>
          <MainText text={props.user.name} customStyle={{marginTop:16, color : '#FFFFFF'}}/>
          <Icons name='create-outline' color={'#FFFFFF'} size={ 20 } style={{margin : 5}} onPress={() => setModalNameVisible(true)}/>
        </View>
      </View>
      <View style={{flexDirection : 'row', justifyContent : 'space-evenly', height : 75}}>
        <View style={{backgroundColor : '#1FCC79', flex:1, alignItems: 'center', alignContent : 'center'}}>
          <SecondaryText text='Total' customStyle={{color : '#FFFFFF', fontSize : 14, marginTop : 5}}/>
          <SecondaryText text={sum} customStyle={{color : '#FFFFFF', marginTop : 5, fontSize : 24}}/>
        </View>
        <View style={{backgroundColor : '#9FA5C0', flex:1, alignItems: 'center', alignContent : 'center'}}>
          <SecondaryText text='Incomplete' customStyle={{color : '#FFFFFF', fontSize : 14, marginTop : 5}}/>
          <SecondaryText text={notCount} customStyle={{color : '#FFFFFF', marginTop : 5, fontSize : 24}}/>
        </View>
      </View>
      <View style={{paddingLeft : 14, paddingRight : 14, marginTop : 24}}>
        <SecondaryText text='Email'customStyle={{textAlign : 'left'}}/>
        <View style={{flexDirection : 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <MainText text={props.user.email} customStyle={{textAlign : 'left', marginTop : 0, fontSize : 20}}/>
          <Icons name='create' color={'#9FA5C0'} size={ 24 } style={{margin : 5}} onPress={() => setModalEmailVisible(true)}/>
        </View>
        <Divider style={{ backgroundColor: '#9FA5C0' }} />
      </View>
      <View style={{paddingLeft : 14, paddingRight : 14}}>
        <SecondaryText text='Age'customStyle={{textAlign : 'left'}}/>
        <View style={{flexDirection : 'row', justifyContent: 'space-between', alignItems: 'center'}}>
          <MainText text={props.user.age} customStyle={{textAlign : 'left', marginTop : 0, fontSize : 20}}/>
          <Icons name='create' color={'#9FA5C0'} size={ 24 } style={{margin : 5}} onPress={() => setModalAgeVisible(true)}/>
        </View>
        <Divider style={{ backgroundColor: '#9FA5C0' }} />
      </View>
      <Button 
        buttonText='Log Out' customStyle={{backgroundColor : '#006D81'}}
        onPress={() => {
          // logOut()
          setModalLogoutVisible(true)
        }}  
      />
      <Button 
        buttonText='Delete User' customStyle={{backgroundColor : '#D00A12', borderColor : '#D00A12', borderWidth : 1, marginTop : 14}}
        onPress={() => {
          // logOut()
          setModalDelVisible(true)
        }}  
      />


      <ModalDelete 
        visible={modalLogoutVisible}
        title='Action Confirmation'
        buttonText='Log Out'
        buttonColor='#006D81' 
        textColor='#FFFFFF' 
        onRequestClose={() => setModalLogoutVisible(!modalLogoutVisible)} 
        onPress={() => {
          removeItemValue()
          setModalLogoutVisible(!modalLogoutVisible);
        }}
      />

      <ModalDelete 
        visible={modalDelVisible}
        title='Delete Account?'
        buttonText='Yes'
        buttonColor='#D00A12' 
        textColor='#FFFFFF'
        onRequestClose={() => setModalDelVisible(!modalDelVisible)} 
        onPress={() => {
          removeAccount()
          setModalLogoutVisible(!modalDelVisible);
        }}
      />
      

      <ModalWithTextBox 
        visible={modalNameVisible} 
        onRequestClose={() => setModalNameVisible(!modalNameVisible)} 
        placeholderText='Edit Your Name' buttonText='OK'
        onChangeText={(value) => setName(value)}
        onPress={() => {
          updateProfile({name : name})
          props.user.name = name
          setModalNameVisible(!modalNameVisible)
          }}/>

      <ModalWithTextBox 
        visible={modalEmailVisible} 
        onRequestClose={() => setModalEmailVisible(!modalEmailVisible)} 
        placeholderText='Edit Your Email' buttonText='OK'
        onChangeText={(value) => setEmail(value)} 
        onPress={() => {
          updateProfile({email : email})
          props.user.email = email
          setModalEmailVisible(!modalEmailVisible)
        }}/>

      <ModalWithTextBox 
        visible={modalAgeVisible} 
        onRequestClose={() => setModalAgeVisible(!modalAgeVisible)} 
        placeholderText='Edit Your Age' 
        buttonText='OK'
        onChangeText={(value) => setAge(value)}  
        onPress={() => {
          updateProfile({age : age})
          props.user.age = age
          setModalAgeVisible(!modalAgeVisible)
          }}/>      
    </View>
  )
}

function mapStateToProps(state) {
    const { payload } = state.authStore;
    return {
      user : payload,
      getAllTaskStore: state.getAllTaskStore,
      registerStore: state.registerStore,
      //getUserStore : state.authStore.user
      //authStore : state.authStore
    }
  }

  
  function mapDispatchToProps(dispatch) {
    return {
      dispatchDirect: (token) => dispatch(fetchGetData(token)),
    }
  }

export default connect(mapStateToProps, mapDispatchToProps) (Profile)