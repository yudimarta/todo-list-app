import axios from 'axios'
import Config from 'react-native-config'

async function fetchUpdate(method, auth, path, id, body){
    try{
        const response = await axios({method,
                                    url : `${Config.BASE_URL}/${path}${id}`,
                                    //url : `https://api-nodejs-todolist.herokuapp.com/task/${id}`,
                                    headers : {'Authorization' : `Bearer ${auth}`},
                                    data : body})
        if (response.status != 400){
            return response
        }
        console.log(response)
    } catch (error){
        throw error
    }
}

export default fetchUpdate