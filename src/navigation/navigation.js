import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import onBoarding from '../Screens/Onboarding.js';
import SignIn from '../Screens/SignIn.js'
import SignUp from '../Screens/SignUp.js'
import Alltask from '../Screens/Alltask.js'
import Activetask from '../Screens/Activetask.js'
import Completedtask from '../Screens/Completedtask.js'
import Profile from '../Screens/Profile.js'
import Icons from 'react-native-vector-icons/Ionicons';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();


function MyTab() {
    return (
        <Tab.Navigator initialRouteName="Home" tabBarOptions={{ activeTintColor : 'white', activeBackgroundColor : '#1FCC79'}}>
          <Tab.Screen 
              name="Alltask" 
              component={Alltask} 
              options={{ tabBarLabel : 'Home', 
              tabBarIcon: ({ color, size}) => (
                  <Icons name="home" color={ color } size={ size } />
              )}}/>
           <Tab.Screen 
              name="Activetask" 
              component={Activetask} 
              options={{ tabBarLabel : 'Active', 
              tabBarIcon: ({ color, size}) => (
                  <Icons name="list" color={ color } size={ size } />
              )}}/>
            <Tab.Screen 
              name="Completedtask" 
              component={Completedtask} 
              options={{ tabBarLabel : 'Done', 
              tabBarIcon: ({ color, size}) => (
                  <Icons name="checkmark-circle" color={ color } size={ size } />
              )}}/>
            <Tab.Screen 
              name="Profile" 
              component={Profile} 
              options={{ tabBarLabel : 'Profile', 
              tabBarIcon: ({ color, size}) => (
                  <Icons name="person" color={ color } size={ size} />
              )}}/>      
        </Tab.Navigator>
    );
  }

const MyStack = () => {
  return (
        <NavigationContainer>
          <Stack.Navigator initialRouteName="onBoarding" headerMode="none">
            <Stack.Screen name="onBoarding" component={onBoarding} />
            <Stack.Screen name="SignIn" component={SignIn} />
            <Stack.Screen name="SignUp" component={SignUp} />
            <Stack.Screen name="Home" component={MyTab}/>
          </Stack.Navigator>
        </NavigationContainer>
  );
}


export default MyStack;