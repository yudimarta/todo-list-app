import React from 'react';
import { Provider } from 'react-redux'

import store from './stores'

import MyStack from '../src/navigation/navigation'

const App = () => {
  return (
    <Provider store={store}>
      <MyStack />
    </Provider>
  );
}

export default App;